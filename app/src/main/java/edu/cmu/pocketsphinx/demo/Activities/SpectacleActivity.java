package edu.cmu.pocketsphinx.demo.Activities;

import android.app.Activity;
import android.content.Intent;
import android.net.Uri;
import android.os.CountDownTimer;
import android.os.Bundle;
import android.view.View;
import android.view.WindowManager;

import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import edu.cmu.pocketsphinx.demo.Classes.FileSetup;
import edu.cmu.pocketsphinx.demo.Classes.Scene;
import edu.cmu.pocketsphinx.demo.R;


public class SpectacleActivity extends Activity {

    SpectacleActivity a = this;
    TextView time;
    TextView textScene;

    Pattern pattern;
    Matcher matcher;
    LinearLayout layout;
    Button btnScenes;

    ArrayList<String> sceneAlreadyPast = new ArrayList();
    ArrayList<String> listImages = new ArrayList();

    CountDownTimer firstTimer;
    Scene sceneAffiche;

    Integer numeroScene=0;
    FileSetup fileSetup;

    public static final String NAMEARRAYLIST = "allscene";
    private static int numGalerie=0;
    WindowManager.LayoutParams lp;

    // all variable to count timer down
    private int timersecDown=50;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_spectacle);
        /**
         *
         */

        textScene = findViewById(R.id.text);
        time = findViewById(R.id.time);
        btnScenes = findViewById(R.id.btnScenes);
        btnScenes.setVisibility(View.GONE);

        btnScenes.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent in = new Intent(getApplicationContext(),AllScenesActivity.class);
                in.putStringArrayListExtra(NAMEARRAYLIST, sceneAlreadyPast);
                startActivity(in);
            }
        });

        fileSetup = new FileSetup("TexteComplet.txt");
        try {
            fileSetup.execute(getApplicationContext());
        } catch (IOException e) {
            e.printStackTrace();
        }
        /**
         * Lancement du Recogniser
         */
       /* PocketSphinx pocketSphinx = new PocketSphinx(this,"bienvenue_sur_le");
        SetupTask setupTask = new SetupTask(this,pocketSphinx);
        setupTask.execute();*/

        // get attributes for set Brightness

        lp = getWindow().getAttributes();
        // disable screen to turn off
        getWindow().getDecorView().setKeepScreenOn(true);

        // disable menu
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
        textScene.setText(R.string.prevention);
        firstTimer = new CountDownTimer(10000,1000) {
            @Override
            public void onTick(long l) {
                time.setText(l/1000+"s");
            }

            @Override
            public void onFinish() {

                sceneSuivante();
            }
        };
        firstTimer.start();
    }

    /**
     * Fonction qui fait defiler les scenes
     */
    public void sceneSuivante(){
        ArrayList<Scene> allScene = new ArrayList();
        allScene = fileSetup.getAllScenes();
        if(numeroScene<allScene.size()){

            // getting scene from ArrayList
            sceneAffiche = allScene.get(numeroScene);

            // add scene in an other ArrayList
            sceneAlreadyPast.add(sceneAffiche.getText());

            // find layout for textView
            layout = findViewById(R.id.layoutScroll);

            // add the text of the scene  to the view
            //textScene.setText(sceneAffiche.getText());
            setTextScene(sceneAffiche.getText(),textScene);

            //fills the list with the list from the chosen scene
            listImages = sceneAffiche.getListeImages();

            // add textView clikable to show pictures
            for (int i=0;i<listImages.size();i++){
                setImages(i,layout);
            }

            final CountDownTimer timerScene = new CountDownTimer(sceneAffiche.getTimer(),1000) {
                @Override
                public void onTick(long l) {
                    getWindow().getDecorView().setSystemUiVisibility(
                            View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                                    | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                                    | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                                    | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                                    | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                                    | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);
                }
                @Override
                public void onFinish() {

                    // setting visible btnAllScene after the first
                    if (numeroScene>=1){
                        btnScenes.setVisibility(View.VISIBLE);
                    }

                    // call this method to beging an other scene
                    sceneSuivante();

                    // set Brightness
                    lp.screenBrightness = 1; // 0.0 - 1.0
                    getWindow().setAttributes(lp);
                }
            };

            final CountDownTimer timerImage = new CountDownTimer(sceneAffiche.getTimer() -3000 ,1000) {
                @Override
                public void onTick(long l) {
                    timersecDown--;
                    if(timersecDown==0){
                        time.setText((l/1000)/60+":0"+timersecDown);
                        timersecDown=60;
                    }
                    if(timersecDown>=10){
                        time.setText((l/1000)/60+":"+timersecDown);
                    }else if (timersecDown<10){
                        time.setText((l/1000)/60+":0"+timersecDown);
                    }
                }
                @Override
                public void onFinish() {
                    time.setText("");
                    textScene.setText("");
                    layout.removeAllViews();
                    // set Brightness
                    lp.screenBrightness = 0; // 0.0 - 1.0
                    getWindow().setAttributes(lp);
                }
            };

            // starting timer
            timerScene.start();
            timerImage.start();

            // increment this for find the next scene
            numeroScene++;

        }else{
            // if all scene allready past, set textView
            // set screen brigthness to max
            lp.screenBrightness = 1; // 0.0 - 1.0
            getWindow().setAttributes(lp);
            time.setText("");
            textScene.setText("FIN merci de votre visite");
        }

    }
    public void setTextScene(String textScene,TextView text){
        Integer numGalerie = 0 ;
        pattern = Pattern.compile(".photo[0-9]_[0-9].");
        matcher = pattern.matcher(textScene);
        String newText = textScene;
        while(matcher.find()){
            numGalerie++;
            String regex = ".photo[0-9]_"+numGalerie+".";
            newText=newText.replaceFirst(regex,"-->");
            text.setText(newText);
        }
        numGalerie=0;
        if (!matcher.find()){
            pattern = Pattern.compile(".photo[0-9][0-9]_[0-9].");
            matcher = pattern.matcher(textScene);
            while(matcher.find()){
                numGalerie++;
                ImageView image = new ImageView(getApplicationContext());
                String regex = ".photo[0-9][0-9]_"+numGalerie+".";
                newText=newText.replaceFirst(regex,"-->");
                text.setText(newText);
            }
            numGalerie=0;

            text.setText(newText);
        }
    }
    public void setImages(int i,LinearLayout layout){
        ImageView image = new ImageView(getApplicationContext());

        image.setImageURI(Uri.parse(listImages.get(i)));
        image.setMinimumHeight(200);
        image.setMinimumWidth(300);

        //adds the image to the view
        layout.addView(image);
    }
}
