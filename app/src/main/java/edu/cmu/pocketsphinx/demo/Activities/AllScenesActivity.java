package edu.cmu.pocketsphinx.demo.Activities;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.LinearLayout;

import java.util.ArrayList;

import edu.cmu.pocketsphinx.demo.R;

public class AllScenesActivity extends Activity {
private ArrayList<String> Scene;
private Bundle bundle;
private Button btnRetour;
private LinearLayout layout;
private LinearLayout thirdLayout;
private int boucle=0;

public static final String NAMEARRAYLIST = "allscene";
public static final String TEXTSCENE = "onescene";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_all_scene);
        bundle=getIntent().getExtras();
        Scene = bundle.getStringArrayList(NAMEARRAYLIST);
        layout = findViewById(R.id.secondeLinear);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        btnRetour = findViewById(R.id.btnretour);
        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        creationBoutton();
    }

    public void creationBoutton(){
        for (int i=0;i<=Scene.size()/4;i++){
            thirdLayout = new LinearLayout(this);
            for (int j=0;j<4;j++){
                if(j+boucle<Scene.size()){
                    if (Scene.get(j+boucle) != null) {
                        Button btnScene = new Button(this);
                        btnScene.setText("scène \n n°" +(j+boucle+1));
                        btnScene.setTextSize(20);
                        final int index = j+boucle;
                        btnScene.setOnClickListener(new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intentOneScene = new Intent(getApplicationContext(),OneSceneActivity.class);
                                intentOneScene.putExtra(TEXTSCENE,Scene.get(index));
                                startActivity(intentOneScene);
                            }
                        });
                        thirdLayout.addView(btnScene);
                    }
                }
            }
            boucle+=4;
            layout.addView(thirdLayout);
        }
    }
}
