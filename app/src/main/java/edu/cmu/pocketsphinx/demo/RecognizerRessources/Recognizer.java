package edu.cmu.pocketsphinx.demo.RecognizerRessources;


import android.media.AudioRecord;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;


import java.io.File;
import java.io.IOException;
import java.util.Collection;
import java.util.HashSet;

import edu.cmu.pocketsphinx.Config;
import edu.cmu.pocketsphinx.Decoder;
import edu.cmu.pocketsphinx.FsgModel;
import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;

public class Recognizer {
    protected static final String TAG = edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.class.getSimpleName();
    private final Decoder decoder;
    private final Decoder decoderBis;
    private final int sampleRate;
    private static final float BUFFER_SIZE_SECONDS = 0.4F;
    private int bufferSize;
    private final AudioRecord recorder;
    private Thread recognizerThread;
    private short[] secondBuffer;
    private static int countDecoder=0;
    private final Handler mainHandler = new Handler(Looper.getMainLooper());
    private final Collection<RecognitionListener> listeners = new HashSet();


    protected Recognizer(Config config) throws IOException {
        this.decoder = new Decoder(config);
        this.decoderBis = new Decoder(config);
        this.sampleRate = (int)this.decoder.getConfig().getFloat("-samprate");
        this.bufferSize = Math.round((float)this.sampleRate * 0.4F);
        this.recorder = new AudioRecord(6, this.sampleRate, 16, 2, this.bufferSize * 2);
        if (this.recorder.getState() == 0) {
            this.recorder.release();
            throw new IOException("Failed to initialize recorder. Microphone might be already in use.");
        }
    }

    public void addListener(RecognitionListener listener) {
        Collection var2 = this.listeners;
        synchronized(this.listeners) {
            this.listeners.add(listener);
        }
    }

    public void removeListener(RecognitionListener listener) {
        Collection var2 = this.listeners;
        synchronized(this.listeners) {
            this.listeners.remove(listener);
        }
    }

    public boolean startListening(String searchName,String searchNameBis) {
        if (null != this.recognizerThread) {
            return false;
        } else {
            Log.i(TAG, String.format("Start recognition \"%s\"", searchName));
            Log.i(TAG, String.format("Start recognition \"%s\"", searchNameBis));
            this.decoder.setSearch(searchName);
            this.decoderBis.setSearch(searchNameBis);
            this.recognizerThread = new edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.RecognizerThread();
            this.recognizerThread.start();
            return true;
        }
    }

    public boolean startListening(String searchName, int timeout) {
        if (null != this.recognizerThread) {
            return false;
        } else {
            Log.i(TAG, String.format("Start recognition \"%s\"", searchName));
            this.decoder.setSearch(searchName);
            this.recognizerThread = new edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.RecognizerThread(timeout);
            this.recognizerThread.start();
            return true;
        }
    }

    private boolean stopRecognizerThread() {
        if (null == this.recognizerThread) {
            return false;
        } else {
            try {
                this.recognizerThread.interrupt();
                this.recognizerThread.join();
            } catch (InterruptedException var2) {
                Thread.currentThread().interrupt();
            }

            this.recognizerThread = null;
            return true;
        }
    }

    public boolean stop() {
        boolean result = this.stopRecognizerThread();
        if (result) {
            Log.i(TAG, "Stop recognition");
            Hypothesis hypothesis=null;

            if(this.decoder.hyp()!=null)
                hypothesis = this.decoder.hyp();
            else if(this.decoderBis.hyp()!=null)
                hypothesis = this.decoderBis.hyp();
            this.mainHandler.post(new edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.ResultEvent(hypothesis, true,hypothesis, true));
        }

        return result;
    }

    public boolean cancel() {
        boolean result = this.stopRecognizerThread();
        if (result) {
            Log.i(TAG, "Cancel recognition");
        }

        return result;
    }

    public Decoder getDecoder() {
        return this.decoder;
    }
    public Decoder getDecoderBis() {
        return this.decoderBis;
    }

    public void shutdown() {
        this.recorder.release();
    }

    public String getSearchName() {
        return this.decoder.getSearch();
    }

    public void addFsgSearch(String searchName, FsgModel fsgModel) {
        this.decoder.setFsg(searchName, fsgModel);
    }

    public void addGrammarSearch(String name, File file) {
        Log.i(TAG, String.format("Load JSGF %s", file));
        this.decoder.setJsgfFile(name, file.getPath());
    }

    public void addGrammarSearch(String name, String jsgfString) {
        this.decoder.setJsgfString(name, jsgfString);
    }

    public void addNgramSearch(String name, File file) {
        Log.i(TAG, String.format("Load N-gram model %s", file));
        this.decoder.setLmFile(name, file.getPath());
    }

    public void addKeyphraseSearch(String name, String phrase,String nameBis, String phraseBis) {
        this.decoder.setKeyphrase(name, phrase);
        this.decoderBis.setKeyphrase(nameBis, phraseBis);
    }

    public void addKeywordSearch(String name, File file) {
        this.decoder.setKws(name, file.getPath());
    }

    public void addAllphoneSearch(String name, File file) {
        this.decoder.setAllphoneFile(name, file.getPath());
    }

    private class TimeoutEvent extends edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.RecognitionEvent {
        private TimeoutEvent() {
            super(null);
        }

        protected void execute(RecognitionListener listener) {
            listener.onTimeout();
        }
    }

    private class OnErrorEvent extends edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.RecognitionEvent {
        private final Exception exception;

        OnErrorEvent(Exception exception) {
            super(null);
            this.exception = exception;
        }

        protected void execute(RecognitionListener listener) {
            listener.onError(this.exception);
        }
    }

    private class ResultEvent extends edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.RecognitionEvent {
        protected final Hypothesis hypothesis;
        protected final Hypothesis hypothesisBis;
        private final boolean finalResult;
        private final boolean finalResultBis;

        ResultEvent(Hypothesis hypothesis, boolean finalResult,Hypothesis hypothesisBis, boolean finalResultBis) {
            super(null);
            this.hypothesis = hypothesis;
            this.finalResult = finalResult;
            this.hypothesisBis = hypothesisBis;
            this.finalResultBis = finalResultBis;
        }

        protected void execute(RecognitionListener listener) {
            if (this.finalResult) {
                listener.onResult(this.hypothesis);
            } else {
                listener.onPartialResult(this.hypothesis);
            }
            if (this.finalResultBis) {
                listener.onResult(this.hypothesisBis);
            } else {
                listener.onPartialResult(this.hypothesisBis);
            }

        }
    }

    private class InSpeechChangeEvent extends edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.RecognitionEvent {
        private final boolean state;

        InSpeechChangeEvent(boolean state) {
            super(null);
            this.state = state;
        }

        protected void execute(RecognitionListener listener) {
            if (this.state) {
                listener.onBeginningOfSpeech();
            } else {
                listener.onEndOfSpeech();
            }

        }
    }

    private abstract class RecognitionEvent implements Runnable {
        private RecognitionEvent(Object o) {
        }

        public void run() {
            RecognitionListener[] emptyArray = new RecognitionListener[0];
            RecognitionListener[] var2 = (RecognitionListener[]) edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.listeners.toArray(emptyArray);
            int var3 = var2.length;

            for(int var4 = 0; var4 < var3; ++var4) {
                RecognitionListener listener = var2[var4];
                this.execute(listener);
            }

        }

        protected abstract void execute(RecognitionListener var1);
    }

    private final class RecognizerThread extends Thread {
        private int remainingSamples;
        private int timeoutSamples;
        private static final int NO_TIMEOUT = -1;

        public RecognizerThread(int timeout) {
            if (timeout != -1) {
                this.timeoutSamples = timeout * edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.sampleRate / 1000;
            } else {
                this.timeoutSamples = -1;
            }

            this.remainingSamples = this.timeoutSamples;
        }

        public RecognizerThread() {
            this(-1);
        }

        public void run() {
            edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.recorder.startRecording();
            if (edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.recorder.getRecordingState() == 1) {
                edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.recorder.stop();
                IOException ioe = new IOException("Failed to start recording. Microphone might be already in use.");
                edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.mainHandler.post(edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.new OnErrorEvent(ioe));
            } else {
                Log.d(edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.TAG, "Starting decoding");
                edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.decoder.startUtt();
                edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.decoderBis.startUtt();
                short[] buffer = new short[edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.bufferSize];
                boolean inSpeech = edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.decoder.getInSpeech();
                boolean inSpeechBis = edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.decoderBis.getInSpeech();
                edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.recorder.read(buffer, 0, buffer.length);

                while(!interrupted() && (this.timeoutSamples == -1 || this.remainingSamples > 0)) {
                    int nread = edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.recorder.read(buffer, 0, buffer.length);
                    if (-1 == nread) {
                        throw new RuntimeException("error reading audio buffer");
                    }

                    if (nread > 0) {
                        if(countDecoder==0){
                            secondBuffer = new short[edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.bufferSize*2];
                            for (int i=0;i<buffer.length;i++){
                                secondBuffer[i]=buffer[i];
                            }

                            edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.decoder.processRaw(buffer, (long)nread, false, false);
                            countDecoder++;
                        }else if(countDecoder==1){
                            for (int i=0;i<buffer.length;i++){
                                secondBuffer[buffer.length-1+i]=buffer[i];
                            }

                            edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.decoder.processRaw(secondBuffer, (long)nread, false, false);
                            countDecoder++;
                        }else{
                            for (int i=0; i<buffer.length;i++){
                                secondBuffer[i]=secondBuffer[buffer.length-1+i];
                                secondBuffer[buffer.length-1+i]=buffer[i];
                            }
                            edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.decoderBis.processRaw(secondBuffer, (long)nread, false, false);
                            edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.decoder.processRaw(secondBuffer, (long)nread, false, false);
                        }

                        if (edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.decoder.getInSpeech() != inSpeech) {
                            inSpeech = edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.decoder.getInSpeech();
                            edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.mainHandler.post(edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.new InSpeechChangeEvent(inSpeech));
                        }
                        if (edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.decoderBis.getInSpeech() != inSpeech) {
                            inSpeech = edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.decoderBis.getInSpeech();
                            edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.mainHandler.post(edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.new InSpeechChangeEvent(inSpeechBis));
                        }
                        if (inSpeech) {
                            this.remainingSamples = this.timeoutSamples;
                        }

                        Hypothesis hypothesis = edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.decoder.hyp();
                        Hypothesis hypothesisBis = edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.decoderBis.hyp();

                        edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.mainHandler.post(edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.new ResultEvent(hypothesis, false,hypothesisBis,false));
                    }

                    if (this.timeoutSamples != -1) {
                        this.remainingSamples -= nread;
                    }
                }

                edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.recorder.stop();
                edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.decoder.endUtt();
                edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.decoderBis.endUtt();
                edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.mainHandler.removeCallbacksAndMessages((Object)null);
                if (this.timeoutSamples != -1 && this.remainingSamples <= 0) {
                    edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.mainHandler.post(edu.cmu.pocketsphinx.demo.RecognizerRessources.Recognizer.this.new TimeoutEvent());
                }

            }
        }
    }
}

