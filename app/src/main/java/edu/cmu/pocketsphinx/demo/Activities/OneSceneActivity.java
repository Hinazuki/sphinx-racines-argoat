package edu.cmu.pocketsphinx.demo.Activities;

import android.app.Activity;
import android.app.Dialog;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.cmu.pocketsphinx.demo.R;

public class OneSceneActivity extends Activity {

    public static final String TEXTSCENE = "onescene";
    private TextView text;
    private String scene;
    private Button btnRetour;
    private ArrayList<String> Listimage;
    private LinearLayout layout;
    private Activity a;
    private static int numGalerie=0;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        a=this;
        setContentView(R.layout.activity_one_scene);
        getWindow().getDecorView().setSystemUiVisibility(
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                        | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                        | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                        | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION // hide nav bar
                        | View.SYSTEM_UI_FLAG_FULLSCREEN // hide status bar
                        | View.SYSTEM_UI_FLAG_IMMERSIVE_STICKY);

        text = findViewById(R.id.textOneScene);
        btnRetour = findViewById(R.id.btnretourbis);
        layout = findViewById(R.id.layoutImage);
        scene = getIntent().getStringExtra(TEXTSCENE);
        Pattern pattern = Pattern.compile(".photo[0-9]_[0-9].");
        Matcher matcher = pattern.matcher(scene);
        Listimage = new ArrayList<>();
        String newText = scene;
        while(matcher.find()){
            numGalerie++;
            String regex = ".photo[0-9]_"+numGalerie+".";
            Listimage.add("/storage/emulated/0/Android/data/edu.cmu.sphinx.pocketsphinx/files/sync/Image/"
                    +scene.substring(matcher.toMatchResult().start()+1,matcher.toMatchResult().end()-1)+".jpg");
            newText=newText.replaceFirst(regex,"(voir photo n°"+numGalerie+")");
        }
        numGalerie=0;
        text.setText(newText);
        if (!matcher.find()){
            pattern = Pattern.compile(".photo[0-9][0-9]_[0-9].");
            matcher = pattern.matcher(scene);
            while(matcher.find()){
                numGalerie++;
                String regex = ".photo[0-9][0-9]_"+numGalerie+".";
                Listimage.add("/storage/emulated/0/Android/data/edu.cmu.sphinx.pocketsphinx/files/sync/Image/"
                        +scene.substring(matcher.toMatchResult().start()+1,matcher.toMatchResult().end()-1)+".jpg");
                newText=newText.replaceFirst(regex,"(voir photo n°"+numGalerie+")");
            }
            numGalerie=0;
            text.setText(newText);
        }

        // add textView clikable for show picture
        for (int i=0;i<Listimage.size();i++){

            ImageView image = new ImageView(getApplicationContext());

            // creat Dialog for show picture
            image.setImageURI(Uri.parse(Listimage.get(i)));
            image.setMinimumHeight(200);
            image.setMinimumWidth(300);

            layout.addView(image);
        }
        btnRetour.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }
}
