package edu.cmu.pocketsphinx.demo.RecognizerRessources;

import android.Manifest;

import android.app.Activity;
import android.content.pm.PackageManager;

import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;


import java.io.File;
import java.io.IOException;


import java.util.HashMap;

import edu.cmu.pocketsphinx.Hypothesis;
import edu.cmu.pocketsphinx.RecognitionListener;
import edu.cmu.pocketsphinx.demo.R;

public class PocketSphinx implements RecognitionListener {
    private static final String KWS_SEARCH = "wakeup";
    private static final String KWS_SEARCHBIS = "second";
    private Activity activity;
    // Keyword we are looking for to activate menu
    private static String keyphrase;
    private static String keyphraseBis;
    private boolean test = false;
    // Used to handle permission request
    private static final int PERMISSIONS_REQUEST_RECORD_AUDIO = 1;

    private static Recognizer recognizer;
    private HashMap<String, Integer> captions;

    public PocketSphinx(Activity activity, String keyphrase, String keyphraseBis){

        this.activity = activity;
        this.keyphrase = keyphrase;
        this.keyphraseBis = keyphraseBis;

        int permissionCheck = ContextCompat.checkSelfPermission(activity.getApplicationContext(), Manifest.permission.RECORD_AUDIO);
        if (permissionCheck != PackageManager.PERMISSION_GRANTED) {
            ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.RECORD_AUDIO}, PERMISSIONS_REQUEST_RECORD_AUDIO);
            return;
        }
        captions = new HashMap<>();
        captions.put(KWS_SEARCH, R.string.kws_caption);
    }


    public void switchSearch(String searchName,String searchNameBis/*,Recognizer reco*/) {

        this.recognizer.stop();
        this.recognizer.startListening(searchName,searchNameBis);

        //String caption = activity.getResources().getString(captions.get(searchName));
    }

    public void setupRecognizer(File assetsDir) throws IOException {
        // The recognizer can be configured to perform multiple searches
        // of different kind and switch between them

        recognizer = RecognizerSetup.defaultSetup()
                .setAcousticModel(new File(assetsDir, "cmusphinx-fr-ptm-8khz-5.2"))
                .setDictionary(new File(assetsDir, "fr.dict"))
                .setRawLogDir(assetsDir) // To disable logging of raw audio comment out this call (takes a lot of space on the device)
                .getRecognizer();
        recognizer.addListener(this);
        // Create keyword-activation search.
        recognizer.addKeyphraseSearch(KWS_SEARCH, this.keyphrase,KWS_SEARCHBIS,this.keyphraseBis);

    }

    /*public void onRequestPermissionsResult(int requestCode,
                                           @NonNull String[] permissions, @NonNull  int[] grantResults) {
        onRequestPermissionsResult(requestCode, permissions, grantResults);

        if (requestCode == PERMISSIONS_REQUEST_RECORD_AUDIO) {
            if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                // Recognizer initialization is a time-consuming and it involves IO,
                // so we execute it in async task

                new SetupTask(activity,this).execute();
            } else {
                activity.finish();
            }
        }
    }*/

    public void onDestroy() {

        if (recognizer != null) {
            recognizer.cancel();
            recognizer.shutdown();
        }
    }

    @Override
    public void onPartialResult(Hypothesis hypothesis) {
        if (hypothesis == null)
            return;

        String text = hypothesis.getHypstr();
        if(text.equals(keyphrase)) {
            switchSearch(KWS_SEARCH,KWS_SEARCHBIS);
        }else if (text.equals(keyphraseBis)){
            switchSearch(KWS_SEARCH,KWS_SEARCHBIS);
        }else{
            System.out.println("mauvais résultat");
        }
    }


    @Override
    public void onResult(Hypothesis hypothesis) {
        if (hypothesis == null)
            return;
        String text = hypothesis.getHypstr();

        if(text.equals(keyphrase)) {
            System.out.println(hypothesis.getHypstr()+"1");
            this.setTest(true);
        }else if (text.equals(keyphraseBis)){
            System.out.println(hypothesis.getHypstr()+"2");
            this.setTest(true);
        }else{
            System.out.println("mauvais résultat");
        }


    }

    @Override
    public void onBeginningOfSpeech() {
    }


    //We stop recognizer here to get a final result

    @Override
    public void onEndOfSpeech() {
        if (!recognizer.getSearchName().equals(KWS_SEARCH))
            switchSearch(KWS_SEARCH,KWS_SEARCHBIS);
    }
    @Override
    public void onError(Exception error) {
        System.out.println(error.getMessage());
    }

    @Override
    public void onTimeout() {
    }

    /**
     * Getters And Setters
     */

    public Activity getActivity() {
        return activity;
    }

    public void setActivity(Activity activity) {
        this.activity = activity;
    }

    public static String getKeyphrase() {
        return keyphrase;
    }

    public static void setKeyphrase(String keyphrase) {
        PocketSphinx.keyphrase = keyphrase;
    }

    public static Recognizer getRecognizer() {
        return recognizer;
    }

    public static void setRecognizer(Recognizer recognizer) {
        PocketSphinx.recognizer = recognizer;
    }

    public boolean isTest() {
        return test;
    }

    public void setTest(boolean test) {
        this.test = test;
    }
}