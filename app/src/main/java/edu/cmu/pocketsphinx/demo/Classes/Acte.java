package edu.cmu.pocketsphinx.demo.Classes;

import java.util.ArrayList;

public class Acte {
        private ArrayList<Scene> listeScene;
        private String nom;
        private int numero;
        private int longueur;


        public Acte(int numero, ArrayList<Scene> listePhrase, String nom){
            this.numero = numero;
            this.listeScene = listePhrase;
            this.nom = nom;
        }
        public Acte(){

        }
        public void supprimerScene(){
            for (int i = 0 ; i < listeScene.size(); i++){
                if (listeScene.get(i).getTest() == true){
                    listeScene.remove(i);
                }
            }
        }

        public ArrayList<Scene> getListeScene() {
            return listeScene;
        }

        public void setListeScene(ArrayList<Scene> listeScene) {
            this.listeScene = listeScene;
        }

        public String getNom() {
            return nom;
        }

        public void setNom(String nom) {
            this.nom = nom;
        }

        public int getNumero() {
            return numero;
        }

        public void setNumero(int numero) {
            this.numero = numero;
        }

        public int getLongueur() {
            return longueur;
        }

        public void setLongueur(int longueur) {
            this.longueur = longueur;
        }

        public String toString(){
            return "Je suis un acte mon nom est " + this.getNom() + " mon numéro est " + this.getNumero() + " je contiens " + this.getListeScene().size() + " scènes";
        }
    }


