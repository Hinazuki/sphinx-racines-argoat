package edu.cmu.pocketsphinx.demo.Classes;


import android.widget.ImageView;

import java.util.ArrayList;

public class Scene {
    private int timer;
    private String reconnaissance;
    private String text;
    private Boolean test;
    private ArrayList<String> listeImages;

    public Scene(int timer,String text, String reconnaissance){
        this.text=text;
        this.reconnaissance = reconnaissance;
        this.timer=timer;
        this.test = false;
    }

    public Scene(){ }

    public int getTimer() {
        return timer;
    }

    public String getText() {
        return text;
    }

    public void setTimer(int timer) {
        this.timer = timer;
    }

    public String getReconnaissance() {
        return reconnaissance;
    }

    public void setReconnaissance(String reconnaissance) {
        this.reconnaissance = reconnaissance;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Boolean getTest() {
        this.setTest(true);
        return test;
    }


    public void setTest(Boolean test) {
        this.test = test;
    }

    public ArrayList<String> getListeImages() {
        return listeImages;
    }

    public void setListeImages(ArrayList<String> listeImages) {
        this.listeImages = listeImages;
    }
    public boolean hasImages(){
        if(this.listeImages.size()>0)
            return true;
        else
            return false;
    }

    public String toString(){
        return "Je suis une scène mon texte est " + this.getText() + " ma reconnaissance est " + this.getReconnaissance()
                + " mon timer est " + this.getTimer() + " j'ai de plus une liste d'image de taille " + this.getListeImages().size();
    }
}
