package edu.cmu.pocketsphinx.demo.Classes;


import android.content.Context;
import android.widget.ImageView;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import edu.cmu.pocketsphinx.Assets;

public class FileSetup {
    private Acte acte;
    private int timerScene;
    private Scene scene;

    Pattern pattern;
    Matcher matcher;

    private String path;
    private ArrayList<Acte> allActes = new ArrayList<>();
    private ArrayList<Scene> allScenes = new ArrayList<>();

    public FileSetup(String path) {
        this.path = path;
    }

    public void execute(Context ctxt) throws IOException {
        Assets assets = new Assets(ctxt);
        File assetDir = assets.syncAssets();
        File file = new File(assetDir, path);
        setScenes(file);
    }

    public void setScenes(File file) throws IOException {
        StringBuilder out;
        String line;
        String readLine;
        String textScene = "";
        FileReader inputreader = new FileReader(file);
        BufferedReader buffreader = new BufferedReader(inputreader);
        Integer numero = 0;
        String reconnaissance = "";
        allScenes = new ArrayList<>();
        while (null != (line = buffreader.readLine())) {
            out = new StringBuilder();
            out.append(line);
            readLine = out.toString();
            if (readLine.substring(0, 2).equals("# ")) {
                timerScene = Integer.parseInt(readLine.substring(2));
            } else if (readLine.substring(0, 2).equals("* ")) {
                numero = Integer.parseInt(readLine.substring(2));
            } else if (readLine.substring(0, 2).equals(": ")) {
                acte = new Acte(numero, new ArrayList<Scene>(), readLine.substring(2));
                allActes.add(acte);
            } else if (readLine.substring(0, 2).equals("+ ")) {
                reconnaissance = readLine.substring(2);
            } else if (readLine.equals("---")) {
                scene = new Scene(timerScene, textScene, reconnaissance);
                scene.setText(textScene);
                textScene = "";
                allScenes.add(scene);
                acte.getListeScene().add(scene);
            } else {
                textScene += readLine;
            }
        }
        for (int i = 0 ; i < allActes.size() ; i++){
            for (int j = 0 ; j < allActes.get(i).getListeScene().size() ; j++){
                getAllActes().get(i).getListeScene().get(j).setListeImages(setImages(allActes.get(i).getListeScene().get(j)));
            }
        }
    }

    public ArrayList setImages(Scene scene){
        ArrayList<String> listImages = new ArrayList<>();
        Integer numGalerie = 0 ;
        pattern = Pattern.compile(".photo[0-9]_[0-9].");
        matcher = pattern.matcher(scene.getText());
        while(matcher.find()){
            numGalerie++;
            listImages.add("/storage/emulated/0/Android/data/edu.cmu.sphinx.pocketsphinx/files/sync/Image/"
                    +scene.getText().substring(matcher.toMatchResult().start()+1,matcher.toMatchResult().end()-1)+".jpg");

        }
        numGalerie=0;
        if (!matcher.find()){
            pattern = Pattern.compile(".photo[0-9][0-9]_[0-9].");
            matcher = pattern.matcher(scene.getText());
            while(matcher.find()){
                numGalerie++;
                listImages.add("/storage/emulated/0/Android/data/edu.cmu.sphinx.pocketsphinx/files/sync/Image/"
                        +scene.getText().substring(matcher.toMatchResult().start()+1,matcher.toMatchResult().end()-1)+".jpg");

            }
            numGalerie=0;
        }
        return listImages;
    }

    public ArrayList<Acte> getAllActes() {
        return allActes;
    }

    public void setAllActes(ArrayList<Acte> allActes) {
        this.allActes = allActes;
    }

    public ArrayList<Scene> getAllScenes() {
        return allScenes;
    }

    public void setAllScenes(ArrayList<Scene> allScenes) {
        this.allScenes = allScenes;
    }

}

