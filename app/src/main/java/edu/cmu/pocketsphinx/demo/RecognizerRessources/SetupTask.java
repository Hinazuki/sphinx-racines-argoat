package edu.cmu.pocketsphinx.demo.RecognizerRessources;

import android.app.Activity;
import android.os.AsyncTask;


import java.io.File;
import java.io.IOException;
import java.lang.ref.WeakReference;

import edu.cmu.pocketsphinx.Assets;

public class SetupTask extends AsyncTask<Void, Void, Exception> {
    private WeakReference<Activity> activityReference;
    private WeakReference<PocketSphinx> pocketSphinx;
    private static final String KWS_SEARCH = "wakeup";
    private static final String KWS_SEARCHBIS = "second";
    public SetupTask(Activity activity, PocketSphinx pocketSphinx) {
        this.activityReference = new WeakReference<>(activity);
        this.pocketSphinx = new WeakReference<>(pocketSphinx);
    }

    @Override
    public Exception doInBackground(Void... voids) {
        try {
            Assets assets = new Assets(activityReference.get());
            File assetDir = assets.syncAssets();
            pocketSphinx.get().setupRecognizer(assetDir);

        } catch (IOException e) {
            return e;
        }
        return null;
    }
    @Override
    public void onPostExecute(Exception result) {
        if (result != null) {
            System.out.println("mauvais résultat");
        } else {
            pocketSphinx.get().switchSearch(KWS_SEARCH,KWS_SEARCHBIS);
        }
    }

}
